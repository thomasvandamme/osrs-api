const express = require('express')
const axios = require('axios')
const app = express()
const port = 3000
const hiscores = ["Overall", "Attack", "Defence", "Strength", "Hitpoints", "Ranged", "Prayer", "Magic", "Cooking", "Woodcutting", "Fletching", "Fishing", "Firemaking", "Crafting", "Smithing", "Mining", "Herblore", "Agility", "Thieving", "Slayer", "Farming", "Runecrafting", "Hunter", "Construction", "League Points", "Bounty Hunter - Hunter", "Bounty Hunter - Rogue", "Clue Scrolls (all)", "Clue Scrolls (beginner)", "Clue Scrolls (easy)", "Clue Scrolls (medium)", "Clue Scrolls (hard)", "Clue Scrolls (elite)", "Clue Scrolls (master)", "LMS - Rank", "Abyssal Sire", "Alchemical Hydra", "Barrows Chests", "Bryophyta", "Callisto", "Cerberus", "Chambers of Xeric", "Chambers of Xerc: Challenge Mode", "Chaos Elemental", "Chaos Fanatic", "Commander Zilyana", "Corporeal Beast", "Crazy Archaeologist", "Dagannoth Prime", "Dagannoth Rex", "Dagannoth Supreme", "Deranged Archaeologist", "General Graardor", "Giant Mole", "Grotesque Guardians", "Hespori", "Kalphite Queen", "King Black Dragon", "Kraken", "Kree'Arra", "K'ril Tsutsaroth", "Mimic", "Nightmare", "Obor", "Sarachnis", "Scorpia", "Skotizo", "The Gauntlet", "The Corrupted Gauntlet", "Theatre of Blood", "Thermonuclear Smoke Devil", "TzKal-Zuk", "TzTok-Jad", "Veneatis", "Vet'ion", "Vorkath", "Wintertodt", "Zalcano", "Zulrah"]

const api_url = 'https://secure.runescape.com/m=hiscore_oldschool/index_lite.ws?player='

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/api/hiscore', (req, res) => {
    if (typeof req.query.player !== 'undefined' && req.query.player) {
        axios.get(api_url.concat(req.query.player))
            .then(response => {
                res.send(parseHiscore(response.data))
            })
            .catch(error => {
                console.log(error)
                res.send('player not found.')
            })
    } else {
        res.send("missing 'player' query.")
    }
})

app.listen(port, () => {
    console.log(`OSRS API online http://localhost:${port}`)
})

function parseHiscore(data) {
    let parsedData = data.split('\n')

    parsedData.forEach(function(element, index, array) {
        array[index] = element.split(',')
    });

    return parseToJSON(parsedData)
}

function parseToJSON(data) {
    let player = {}
    let isSkill = 3;

    for (let x = 0; x < data.length; x++) {
        player[hiscores[x]] = {}
        for (let y = 0; y < data[x].length; y++) {
            switch (y) {
                case 0:
                    player[hiscores[x]].rank = data[x][y]
                    break;
                case 1:
                    if (data[x].length < isSkill) {
                        player[hiscores[x]].count = data[x][y]
                    } else {
                        player[hiscores[x]].level = data[x][y]
                    }
                    break;
                case 2:
                    player[hiscores[x]].exp = data[x][y]
                    break;
            }
        }
    }

    return player
}